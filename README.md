# Fatigue Science test
## Instructions
Create a REST service in node.js (using either JSON or XML)

Input : Takes an address with the following separate fields:
* street
* city
* state
* country

Validation : If any input params are not passed in, return a 400 status code with the appropriate error message

Output :  If validation passes, returns 200 status code with the following fields:
* longitude
* latitude
* elevation
* time zone (Id)
* UTC offset (hours)

Guidance:

Use Google Maps API:
https://developers.google.com/maps/documentation/webservices/

Services required:
Geocoding, Elevation, and Timezone APIs
Call services in most efficient manner possible.  e.g. simultaneously when possible.

## Running the Server
* npm install
* npm start
