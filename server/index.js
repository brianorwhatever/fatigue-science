var express = require('express');
var request = require('request');
var Promise = require('bluebird');
var config = require('./config');

// Initialize App
var app = express();
app.get('/location', getLocation);

function geocode(street, city, state, country) {
  /* Get the Geocode */
  return new Promise((resolve, reject) => {
    request(config.geocodeUrl+`?address=${street},+${city},+${state},+${country}&key=${config.googleApiKey}`,
      (error, response, body) => {
        const bodyInJson = JSON.parse(body);
        if(bodyInJson.status === 'OK')
          resolve({lat: bodyInJson.results[0].geometry.location.lat, lng: bodyInJson.results[0].geometry.location.lng});
        else
          reject("Geocode API error");
      }
    );
  });
}

function elevation(lat, lng) {
  // /* Get the elevation */
  return new Promise((resolve, reject) => {
    request(config.elevationUrl+`?locations=${lat},${lng}&key=${config.googleApiKey}`,
      (error, response, body) => {
        const bodyInJson = JSON.parse(body);
        if(bodyInJson.status === 'OK')
          resolve(bodyInJson.results[0].elevation);
        else
          reject("Elevation API error");
      }
    );
  });
}

function timezone(lat, lng) {
  /* Get the timezone */
  const timestamp = Math.floor(Date.now()/1000);
  return new Promise((resolve, reject) => {
    request(config.timezoneUrl+`?location=${lat},${lng}&timestamp=${timestamp}&key=${config.googleApiKey}`,
      (error, response, body) => {
        const bodyInJson = JSON.parse(body);
        if(bodyInJson.status === 'OK')
          resolve({timeZoneId: bodyInJson.timeZoneId, utcOffset: bodyInJson.rawOffset/60/60});
        else
          reject("Timezone API error");
      }
    );
  });
}

function validateParams(query) {
  let errors = [];
  if(typeof query['street'] === 'undefined')
    errors.push('Street is required')
  if(typeof query['city'] === 'undefined')
    errors.push('City is required');
  if(typeof query['country'] === 'undefined')
    errors.push('Country is required');
  if(typeof query['state'] === 'undefined')
    errors.push('State is required');

  return errors;
}

function getLocation(req, res) {
  var errors = validateParams(req.query);

  if(errors.length === 0) {
    /* Run Geocode API request */
    geocode(req.query['street'], req.query['city'], req.query['state'], req.query['country'])
      .then((location) => {
        let returnVal = {status: 'success', data: {longitude: location.lng, latitude: location.lat}};

        /* Run elevation API request */
        const elevationPromise = elevation(location.lat, location.lng)
          .then((elevation) => {
            returnVal.data.elevation = elevation;
          })
          .catch((error) => {
            console.log(error);
          });

        /* Run timezone API request */
        const timezonePromise = timezone(location.lat, location.lng)
          .then((timezone) => {
            returnVal.data.timeZoneId = timezone.timeZoneId;
            returnVal.data.utcOffset = timezone.utcOffset;
          })
          .catch((error) => {
            console.log(error);
          });

        /* Data ready, send the response */
        Promise.all([elevationPromise, timezonePromise]).then(function() {
          res.send(returnVal);
        });
      })
      .catch((error) => {
        res.status(500);
        res.send({status: 'error', errors: [error]});
      });
  } else {
    res.status(400);
    res.send({status: 'error', errors: errors});
  }

}

app.listen(3000, function() {
  console.log('Listening on port 3000');
})
